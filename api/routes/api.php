<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => '\App\Http\Controllers\API'],function($router) {

	/**
	 * Non Authenticated endpoints
	 */
	Route::post('register', 'RegisterController@store')->name('api.register');
	Route::post('login', 'AuthController@login')->name('api.login');
	Route::post('logout', 'AuthController@logout')->name('api.logout');
	Route::apiResource('posts','PostController')->only(['index','show']);
	Route::apiResource('posts.comments','PostCommentController')->only(['index','show']);

	/**
	 * Authenticated endpoints
	 */
	Route::group(['middleware' => ['auth:api']],function($router){
		Route::apiResource('posts','PostController')->only(['store','update','destroy']);
		Route::apiResource('posts.comments','PostCommentController')->only(['store','update','destroy']);
	});

	
});

