<?php

namespace App\Http\Controllers\API;

use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $guard = 'api';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $token = '';
        request()->validate([
            'email' => [
                'required',
                'email',
                function($attribute, $value, $fail){

                    if ( ! $token = auth($this->guard)->attempt(request(['email', 'password']))) {
                        $fail('These credentials do not match our records.');
                    }
                }
            ],
            'password' => 'required'
        ]);

        $token = auth($this->guard)->attempt(request(['email', 'password']));
        
        return response()->json([
            'token' => $token,
            'token_type' => 'bearer',
            'expires_at' => Carbon::now()->addSeconds(auth($this->guard)->factory()->getTTL() * 60)->format('Y-m-d H:i:s')
        ]);
    }

    public function logout()
    {
        auth($this->guard)->logout();
    }
}
