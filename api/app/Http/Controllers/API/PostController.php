<?php

namespace App\Http\Controllers\API;

use App\Post;
use App\Http\Resources\Posts as PostResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostController extends Controller
{

    protected $guard = 'api';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PostResource::collection(Post::latest()->paginate(15));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'string|required',
            'content' => 'string|required',
            
        ]);

        $data = [
            'title' => $request->input('title'),
            'content' => $request->input('content'),
            'image' => $request->input('image',null),
            'user_id' => auth()->guard($this->guard)->user()->id
        ];

        $post = Post::create($data);

        return response()->json($post,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return new PostResource($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title' => 'string|required',
            'content' => 'string|required',
            
        ]);

        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->image = $request->input('image',null);

        $post->save();

        return new PostResource($post);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if ( $post->delete()) {
            return response()->json(['status' => 'record deleted successfully']);
        }

        return response()->json(['status' => 'failed to deleted record'],422);
    }
}
