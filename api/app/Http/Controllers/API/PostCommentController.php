<?php

namespace App\Http\Controllers\API;

use App\Comment;
use App\Post;
use App\Http\Resources\Comments as CommentResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostCommentController extends Controller
{
    protected $guard = 'api';

    /**
     * Display a listing of the resource.
     *
     * @param  App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function index(Post $post)
    {
        return new CommentResource($post->comments);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Post $post)
    {
        $request->validate([
            'body' => 'string|required'
            
        ]);

        return new CommentResource($post->comments()->create([
            'body'=>$request->input('body'),
            'creator_id' => auth()->guard($this->guard)->user()->id
            ])
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        return new CommentResource($comment);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\Post $post
     * @param  App\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post, Comment $comment)
    {
        $request->validate([
            'body' => 'string|required'
            
        ]);

        $comment->body = $request->input('body');
        $comment->save();

        return new CommentResource($comment);
    }

    /**
     * Remove the specified resource from storage.
     * @param  App\Post $post
     * @param  App\Comment $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy( Post $post, Comment $comment)
    {
        if ( $comment->delete()) {
            return response()->json(['status' => 'record deleted successfully']);
        }

        return response()->json(['status' => 'failed to deleted record'],422);
    }
}
